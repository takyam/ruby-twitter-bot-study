# -*- encoding: utf-8 -*-
require 'rubygems'
require 'twitter'
require 'mongo'

load './config.rb',true
Twitter.configure do |conf|
  conf.consumer_key = $config[:consumer_key]
  conf.consumer_secret = $config[:consumer_secret]
  conf.oauth_token = $config[:oauth_token]
  conf.oauth_token_secret = $config[:oauth_token_secret]
end

#generate message
value1 = rand(10000) + 1
value2 = rand(10000) + 1

method = rand(5)

result_view = '='
case method
when 0
  result = value1 + value2
  method_view = '+'
when 1
  result = value1 - value2
  method_view = '-'
when 2
  result = value1 * value2
  method_view = '×'
when 3
  result = (value1.to_f / value2.to_f).round(5)
  method_view = '÷'
when 4
  result = value1 % value2
  method_view = '÷'
  result_view = 'のあまりは'
end

message = value1.to_s + ' ' + method_view + ' ' + value2.to_s + ' ' + result_view + ' ' + result.to_s
p message
#Twitter.update message

@conn = Mongo::Connection.new
@db   = @conn['twitter-db']
@coll = @db['tweets']

#とりあえず最新の関連する発言をもろもろ取得して、
# Mongodbに入ってないstr_idのものは新しくINSERTする
Twitter.search('#ruby', :lang => 'ja', :rpp => 5).results.map do |status|
  hash = {}
  status.instance_variables.each {|var| hash[var.to_s.delete("@")] = status.instance_variable_get(var) }
  if @coll.find('id_str' => hash['attrs'][:id_str]).to_a.count > 0
    next
  end
  hash['attrs'][:tweeted] = false
  @coll.insert(hash['attrs'])
end

# mongoをぐぐって、repliedフラグがオフのやつに対してリプライする